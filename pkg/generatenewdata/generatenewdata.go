package generatenewdata

import (
	"math/rand"
	"time"

	"gitlab.com/remotejob/mlengine/markgenclass"
)

func Generate(in string) string {

	var mres string

	markgenclass.Init()
	rand.Seed(time.Now().UnixNano())
	markgenclass.MainChain.Build(in)
	mres = markgenclass.MainChain.Generate()

	return mres

}
