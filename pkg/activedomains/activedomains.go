package activedomains

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/remotejob/mlfactory-feederv5/internal/domains"
)

const (
	layoutISO = "2006-01-02"
)

func Get(link string) ([]string, error) {

	var result []domains.Activedomain
	myClient := &http.Client{Timeout: 10 * time.Second}
	resp, err := myClient.Get(link)
	if err != nil {

		return nil, err
	}
	defer resp.Body.Close()

	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {

		return nil, err
	}
	tnow := time.Now()

	var retdoms []string

	for _, dom := range result {

		t, err := time.Parse(layoutISO, dom.Created)
		if err != nil {
			return nil, err
		}

		if tnow.Sub(t).Hours() < 4500 { //about 6 mounth

			retdoms = append(retdoms, dom.Domain)

		}
	}

	return retdoms, nil
}
