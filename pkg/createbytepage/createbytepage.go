package createbytepage

import (
	"html/template"
	"log"

	"gitlab.com/remotejob/mlfactory-feederv5/internal/common"
	"gitlab.com/remotejob/mlfactory-feederv5/internal/config"
	"gitlab.com/remotejob/mlfactory-feederv5/internal/domains"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/createitem"
	"gitlab.com/remotejob/mlfactory-feederv5/pkg/titlelinks"
)


func CreateMany(remdatas []domains.Phrase, conf *config.Config, orgphrases []string, activedom []string) []domains.Item {

	var retval []domains.Item

	// log.Println("from remote", len(remdatas))

	for _, gendata := range remdatas {

		var res domains.Item

		titlelinksset := titlelinks.CreateForOnePage(gendata.Title, orgphrases, activedom)

		var page domains.Page
		var pageBytes []byte
		var err error

		page.Phrase = template.HTML(gendata.Txt)
		page.Title = titlelinksset.Title
		page.Links = titlelinksset.Links

		varn := common.Random(0, conf.Constants.Variants.Quant)

		if varn == 0 {

			page.Jscript = conf.Constants.Variants.Js0
			pageBytes, err = createitem.Create(conf.Variant0, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		} else if varn == 1 {

			page.Jscript = conf.Constants.Variants.Js1
			pageBytes, err = createitem.Create(conf.Variant1, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		} else if varn == 2 {

			page.Jscript = conf.Constants.Variants.Js2
			pageBytes, err = createitem.Create(conf.Variant2, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		} else if varn == 3 {

			page.Jscript = conf.Constants.Variants.Js3
			pageBytes, err = createitem.Create(conf.Variant3, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		} else if varn == 4 {

			page.Jscript = conf.Constants.Variants.Js4
			pageBytes, err = createitem.Create(conf.Variant4, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		} else if varn == 5 {

			page.Jscript = conf.Constants.Variants.Js5
			pageBytes, err = createitem.Create(conf.Variant5, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		} else if varn == 6 {

			page.Jscript = conf.Constants.Variants.Js6
			pageBytes, err = createitem.Create(conf.Variant6, "index", page)
			if err != nil {

				log.Fatalln(err)
			}

		}
		res.Page = pageBytes

		retval = append(retval, res)

	}

	return retval

}
