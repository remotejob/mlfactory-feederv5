package domains

import "html/template"

type Phrase struct {
	Rowid int64

	Title string

	Txt string
}

type Page struct {
	Title   string
	// Phrase  string
	Phrase template.HTML
	Links   []string
	Images  []string
	Extlink string
	Jscript string
}

type TitleLinks struct {
	Title string
	Links []string
}

type Activedomain struct {

	Domain string
	Created string
}

type RemoteData struct {

	Title string
	Txt string
}

type Item struct {
	Page []byte
}